package by.step.it.j2018.bank.mt.model;

public class Account {
	private static long idCounter;
	private long id;
	private double value;

	public Account(double d) {
		value = d;
		id = idCounter++;
	}

	public boolean deposit(double amount) {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			//never gonna happen
		}
		value += amount;
		return Math.random() < 0.00001;
	}
	
	public boolean withdraw(double amount) {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			//never gonna happen
		}
		if (value>amount) {
			value-=amount;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean transfer(double amount, Account target) {
		if (withdraw(amount)) {
			if (target.deposit(amount)) {
				return true;
			} else {
				deposit(amount);
				return false;
			}
		} else {
			return false;
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", value=" + value + "]";
	}

	public static Account generateFakeAccount() {
		return new Account(500 + Math.random() * 1500);
	}
}
